using System;
using System.Collections.Generic;
using ClassJournal.Models;

namespace ClassJournal.Web.Models
{
	public class StudentsMarks
	{
		public DateTime FirstDate;
		public DateTime LastDate;
		public DateTime PreviousPeriodFirstDate;
		public DateTime PreviousPeriodLastDate;
		public DateTime NextPeriodFirstDate;
		public DateTime NextPeriodLastDate;
		public List<StudentMarks> Students;
		public bool NextPeriodExists;

		public StudentsMarks(DateTime lastDate, List<Student> students)
		{
			LastDate = lastDate;
			FirstDate = LastDate.AddDays(-32);
			PreviousPeriodFirstDate = FirstDate.AddDays(-33);
			PreviousPeriodLastDate = FirstDate.AddDays(-1);
			NextPeriodFirstDate = LastDate.AddDays(1);
			NextPeriodLastDate = LastDate.AddDays(33);
			if (NextPeriodLastDate > DateTime.Today)
			{
				NextPeriodLastDate = DateTime.Today;
				NextPeriodFirstDate = NextPeriodLastDate.AddDays(-32);
			}

			NextPeriodExists = NextPeriodLastDate > LastDate;

			Students = new List<StudentMarks>();

			foreach (var student in students)
				Students.Add(new StudentMarks(student, FirstDate, LastDate));
			foreach (var mark in StudentMark.SelectAll())
			foreach (var student in Students)
				if (student.TryAdd(mark))
					break;
		}
	}
}