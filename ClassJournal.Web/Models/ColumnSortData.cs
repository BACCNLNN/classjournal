using ClassJournal.Models;

namespace ClassJournal.Web.Models
{
	public class ColumnSortData
	{
		public readonly string Name;
		public readonly StudentsSort FieldName;
		public readonly StudentsSort CurrentSort;

		public ColumnSortData(StudentsSort fieldName, StudentsSort currentSort)
		{
			FieldName = fieldName;
			CurrentSort = currentSort;
			
			var member = typeof(StudentsSort).GetField(FieldName.ToString());
			var attribute = (SortAttribute)member.GetCustomAttributes(typeof(SortAttribute), false)[0];
			Name = attribute.Name;
		}
	}
}