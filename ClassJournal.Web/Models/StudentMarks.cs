﻿using System;
using System.Collections.Generic;
using ClassJournal.Models;

namespace ClassJournal.Web.Models
{
	public class StudentMarks
	{
		public DateTime FirstDate;
		public DateTime LastDate;
		public Student Student;
		public int[] Marks;

		public StudentMarks(Student student, DateTime firstDate, DateTime lastDate)
		{
			Student = student;
			FirstDate = firstDate;
			LastDate = lastDate;
			Marks = new int[(LastDate - FirstDate).Days+1];
		}

		public bool TryAdd(StudentMark mark)
		{
			if (mark == null) throw new ArgumentNullException(nameof(mark));
			if (mark.Date == null)
				return false;
			if (mark.Date.Value.Date > LastDate)
				return false;
			if (mark.Date.Value.Date < FirstDate)
				return false;
			if (mark.StudentId != Student.Id)
				return false;

			Marks[(mark.Date.Value - FirstDate).Days] = mark.Mark;
			return true;
		}
	}
}