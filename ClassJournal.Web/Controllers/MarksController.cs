﻿using System;
using System.Globalization;
using ClassJournal.Models;
using ClassJournal.Web.Models;
using Microsoft.AspNetCore.Mvc;

namespace ClassJournal.Web.Controllers
{
	public class MarksController : Controller
	{
		public IActionResult Index(string lastDate)
		{
			DateTime date;
			if (string.IsNullOrEmpty(lastDate))
				date = DateTime.Today;
			else if (!DateTime.TryParseExact(lastDate, "dd.MM.yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
				return BadRequest();
			else if (date > DateTime.Today)
				date = DateTime.Today;

			var model = new StudentsMarks(date, Student.SelectAll(StudentsSort.FirstName));
			return View(model);
		}

		[HttpPost]
		public IActionResult Add(int id, int mark)
		{
			foreach (var studentMark in StudentMark.SelectByStudentId(id))
			{
				if (mark == 0)
				{
					studentMark.Delete();
				}

				else if (studentMark.Date == DateTime.Today)
				{
					studentMark.Mark = mark;
					studentMark.Update();
				}
				break;
			}

			new StudentMark
			{
				Mark = mark,
				StudentId = id,
				Date = DateTime.Now
			}.Insert();
			return RedirectToAction("Index");
		}
	}
}
