﻿using System;
using System.Globalization;
using ClassJournal.Models;
using ClassJournal.Web.Models;
using Microsoft.AspNetCore.Mvc;

namespace ClassJournal.Web.Controllers
{
	public class StudentsController : Controller
    {
        public IActionResult Index(StudentsSort sort)
        {
	        var model = Student.SelectAll(sort);
	        ViewBag.Sort = sort;
            return View(model);
        }

	    public IActionResult Edit(int id = 0)
	    {
		    var model = id > 0 ? Student.GetById(id) : new Student();
		    ViewBag.IsAjax = Request.Method == "POST";
		    return View(model);
	    }

	    [HttpPost]
	    public IActionResult Update(int id, string firstName, string lastName, string birthDate)
	    {
		    var model = id > 0 ? Student.GetById(id) : new Student();
		    model.FirstName = firstName;
		    model.LastName = lastName;
		    if (DateTime.TryParseExact(birthDate, "yyyy-MM-dd", CultureInfo.CurrentCulture, DateTimeStyles.None, out var result))
			    model.Birthdate = result;
		    else
			    model.Birthdate = null;

		    if (id > 0)
			    model.Update();
		    else
			    model.Insert();

		    return RedirectToAction("Index");
	    }

	    [HttpPost]
		public IActionResult Delete(int id)
	    {
		    var model = Student.GetById(id);
			model.Delete();
		    foreach (var studentMark in StudentMark.SelectByStudentId(id))
		    {
				studentMark.Delete();
			}

		    return RedirectToAction("Index");
		}
	}
}
