﻿using System;
using System.Globalization;
using System.Linq;
using ClassJournal.Models;

namespace ClassJournal
{
	class Program
	{
		static Config _config;

		static void Main(string [] args)
		{
			var filename = args.FirstOrDefault() ?? "config.json";
			try
			{
				_config = Config.Load(filename);
				DataBase.ConnectionString = _config.ConnectionString;
			}
			catch (Exception e)
			{
				Console.WriteLine($"Ошибка чтения файла ({filename}): {e.Message}");
				return;
			}

//			while (true)
//			{
//				Console.WriteLine("CLASSJOURNAL");

//				foreach (var student in Student.SelectAll(_config.StudentsSort))
//					Console.WriteLine(student);

//				//foreach (var student in StudentWithMarks.SelectAll())
//				//	Console.WriteLine(student);

//				while (true)
//				{
//					Console.Write(@"Введите:
//S для сортировки журнала
//Id студента для работы со студентом
//N для добавления нового студента: ");
//					var input = Console.ReadLine()?.ToLower();
//					if (input == "s")
//					{
//						while (true)
//						{
//							Console.Write(@"Введите цифру:
//1 для сортировки по имени
//2 для сортировки по дате рождения
//3 для сортировки по среднему баллу
//4 для сортировки попоследней оценке: ");
							
//							if (int.TryParse(Console.ReadLine(), out var number))
//							{
//								switch (number)
//								{
//								case 1:
//									_config.StudentsSort = "first_name";
//									break;
//								case 2:
//									_config.StudentsSort = "birthdate";
//									break;
//								case 3:
//									_config.StudentsSort = "avg_mark";
//									break;
//								case 4:
//									_config.StudentsSort = "last_mark";
//									break;
//								default:
//									Console.WriteLine("Введите число от 1 до 4.");
//									continue;
//								}
//								_config.Save();
//								break;
//							}
//							Console.WriteLine("Введите число от 1 до 4.");
//						}
//						break;
//					}
//					if (input == "n")
//					{
//						NewStudent();
//						break;
//					}
//					if (input == "q")
//						return;

//					if (int.TryParse(input, out var id))
//					{
//						var student = Student.GetById(id);
//						if (student == null)
//						{
//							Console.WriteLine("Студента с таким Id не существует.");
//							continue;
//						}

//						StudentSelected(student);
//						break;
//					}
//					Console.WriteLine("Неверная команда.");
//				}
//			}
		}

		static void NewStudent()
		{
			var newStudent = new Student();
			EditStudent(newStudent);

			newStudent.Insert();
		}

		static void EditStudent(Student student)
		{
			Console.Write("Введите имя: ");
			student.FirstName = Console.ReadLine();
			Console.Write("Введите фамилию: ");
			student.LastName = Console.ReadLine();

			while (true)
			{
				Console.Write("Введите дату рождения (дд.мм.гггг): ");
				var input = Console.ReadLine();
				if (string.IsNullOrWhiteSpace(input))
				{
					student.Birthdate = null;
					break;
				}
				if (DateTime.TryParseExact(input, "dd.MM.yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out var dt))
				{
					student.Birthdate = dt;
					break;
				}
				Console.WriteLine("Неправильный формат даты.");
			}
		}

		static void DeleteStudent(Student student)
		{
			while (true)
			{
				Console.WriteLine(@"Вы уверены, что хотите удалить запись?
Y - удалить
N - отмена");
				var input = Console.ReadLine()?.ToLower();
				if (input == "y")
				{
					student.Delete();
					break;
				}

				return;
			}
		}

		static void AddMark(Student student)
		{
			while(true)
			{
				Console.WriteLine("Текущие оценки студента");
				foreach (var studentMark in StudentMark.SelectByStudentId(student.Id))
					Console.WriteLine(studentMark);

				Console.Write("Поставьте оценку: ");
				if (int.TryParse(Console.ReadLine(),out var mark))
				{
					if (mark < 6 && mark > 0)
					{
						student.AddMark(mark);
						break;
					}
				}
				return;
			}
		}


		static void StudentSelected(Student student)
		{
			while (true)
			{
				Console.WriteLine("STUDENT");

				Console.WriteLine(student);
				foreach (var studentMark in student.Marks)
					Console.WriteLine(studentMark);

				while (true)
				{	
					Console.WriteLine(@"Введите:
E - редактировать студента
D - удалить студента
M - поставить оценку
R - вернуться к списку");

					var input = Console.ReadLine()?.ToLower();

					switch(input)
					{
					case "e":
						EditStudent(student);
						student.Update();
						break;
					case "d":
						DeleteStudent(student);
						return;
					case "m":
						AddMark(student);
						break;
					case "r":
						return;
					default:
						Console.WriteLine("Неверная команда");
						continue;
					}
					break;
				}
			}
		}

		// ReSharper disable once UnusedMember.Local
		static void Trash()
		{
			Console.WriteLine(Student.GetById(9)); // Получить одного студента по его Id

			var newStudent = new Student
			{
				FirstName = "Иван",
				LastName = "Иванов",
				Birthdate = new DateTime(1994, 3, 12)
			};

			newStudent.Insert(); // Вставить студента в таблицу

			var stud = Student.GetById(2);
			stud.LastName = "Кузнецов";
			stud.Update();

			stud = Student.GetById(9);
			stud?.Delete();

			//foreach (var student in Student.SelectAll())
				//Console.WriteLine(student);

			var newStudentMark = new StudentMark
			{
				Mark = 3,
				StudentId = 2,
				Date = new DateTime(1994, 3, 12)
			};

			newStudentMark.Insert(); // Вставить студента в таблицу

			var studMark = StudentMark.GetById(2);
			studMark.Mark = 1;
			studMark.Update();

			studMark = StudentMark.GetById(6);
			studMark?.Delete();

			foreach (var studentMark in StudentMark.SelectAll())
				Console.WriteLine(studentMark);

			foreach (var studentMark in StudentMark.SelectByStudentId(1))
				Console.WriteLine(studentMark);

			stud = Student.GetById(2);
			stud.AddMark(5);

			foreach (var studentMark in stud.Marks)
				Console.WriteLine(studentMark);

			Console.ReadKey();
		}
	}
}
