﻿
using System.IO;
using Newtonsoft.Json;

namespace ClassJournal
{
	class Config
	{
		public string ConnectionString { get; set; }
		public string StudentsSort { get; set; }

		string _filename;

		//"Server=127.0.0.1;Port=5432;Database=students;User Id=students_client; Password = 12345;"

		public void Save()
		{
			var json = JsonConvert.SerializeObject(this, Formatting.Indented);
			File.WriteAllText(_filename, json);
		}

		public static Config Load(string filename)
		{
			var config = JsonConvert.DeserializeObject<Config>(File.ReadAllText(filename));
			config._filename = filename;
			return config;
		}
	}

}

