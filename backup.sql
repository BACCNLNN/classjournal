--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.10
-- Dumped by pg_dump version 9.5.10

-- Started on 2018-10-07 20:08:27

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12355)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2120 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 182 (class 1259 OID 16397)
-- Name: students; Type: TABLE; Schema: public; Owner: students_client
--

CREATE TABLE students (
    id integer NOT NULL,
    first_name character varying,
    last_name character varying,
    birthdate timestamp without time zone
);


ALTER TABLE students OWNER TO students_client;

--
-- TOC entry 181 (class 1259 OID 16395)
-- Name: students_id_seq; Type: SEQUENCE; Schema: public; Owner: students_client
--

CREATE SEQUENCE students_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE students_id_seq OWNER TO students_client;

--
-- TOC entry 2121 (class 0 OID 0)
-- Dependencies: 181
-- Name: students_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: students_client
--

ALTER SEQUENCE students_id_seq OWNED BY students.id;


--
-- TOC entry 184 (class 1259 OID 16409)
-- Name: students_marks; Type: TABLE; Schema: public; Owner: students_client
--

CREATE TABLE students_marks (
    id integer NOT NULL,
    mark integer NOT NULL,
    student_id integer NOT NULL,
    date timestamp without time zone
);


ALTER TABLE students_marks OWNER TO students_client;

--
-- TOC entry 183 (class 1259 OID 16407)
-- Name: students_marks_id_seq; Type: SEQUENCE; Schema: public; Owner: students_client
--

CREATE SEQUENCE students_marks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE students_marks_id_seq OWNER TO students_client;

--
-- TOC entry 2122 (class 0 OID 0)
-- Dependencies: 183
-- Name: students_marks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: students_client
--

ALTER SEQUENCE students_marks_id_seq OWNED BY students_marks.id;


--
-- TOC entry 1988 (class 2604 OID 16400)
-- Name: id; Type: DEFAULT; Schema: public; Owner: students_client
--

ALTER TABLE ONLY students ALTER COLUMN id SET DEFAULT nextval('students_id_seq'::regclass);


--
-- TOC entry 1989 (class 2604 OID 16412)
-- Name: id; Type: DEFAULT; Schema: public; Owner: students_client
--

ALTER TABLE ONLY students_marks ALTER COLUMN id SET DEFAULT nextval('students_marks_id_seq'::regclass);


--
-- TOC entry 2110 (class 0 OID 16397)
-- Dependencies: 182
-- Data for Name: students; Type: TABLE DATA; Schema: public; Owner: students_client
--

INSERT INTO students (id, first_name, last_name, birthdate) VALUES (1, 'Василий', 'Васильев', NULL);
INSERT INTO students (id, first_name, last_name, birthdate) VALUES (3, 'Сергей', 'Сергеев', NULL);
INSERT INTO students (id, first_name, last_name, birthdate) VALUES (5, 'Григорий', 'Григорьев', NULL);
INSERT INTO students (id, first_name, last_name, birthdate) VALUES (6, 'Петров', 'Петр', '1993-03-12 00:00:00');
INSERT INTO students (id, first_name, last_name, birthdate) VALUES (7, 'Петр', 'Сидоров', '1993-03-12 00:00:00');
INSERT INTO students (id, first_name, last_name, birthdate) VALUES (10, 'Иван', 'Иванов', NULL);
INSERT INTO students (id, first_name, last_name, birthdate) VALUES (11, 'Иван', 'Иванов', NULL);
INSERT INTO students (id, first_name, last_name, birthdate) VALUES (12, 'Иван', 'Иванов', NULL);
INSERT INTO students (id, first_name, last_name, birthdate) VALUES (13, 'Петров', 'Петр', NULL);
INSERT INTO students (id, first_name, last_name, birthdate) VALUES (14, 'Иван', 'Иванов', NULL);
INSERT INTO students (id, first_name, last_name, birthdate) VALUES (16, 'Иван', 'Иванов', '1994-03-12 00:00:00');
INSERT INTO students (id, first_name, last_name, birthdate) VALUES (17, 'Иван', 'Иванов', '1994-03-12 00:00:00');
INSERT INTO students (id, first_name, last_name, birthdate) VALUES (18, 'Иван', 'Иванов', '1994-03-12 00:00:00');
INSERT INTO students (id, first_name, last_name, birthdate) VALUES (19, 'Иван', 'Иванов', '1994-03-12 00:00:00');
INSERT INTO students (id, first_name, last_name, birthdate) VALUES (20, 'Иван', 'Иванов', '1994-03-12 00:00:00');
INSERT INTO students (id, first_name, last_name, birthdate) VALUES (21, 'Иван', 'Иванов', '1994-03-12 00:00:00');
INSERT INTO students (id, first_name, last_name, birthdate) VALUES (22, 'Иван', 'Иванов', '1994-03-12 00:00:00');
INSERT INTO students (id, first_name, last_name, birthdate) VALUES (23, 'Иван', 'Иванов', '1994-03-12 00:00:00');
INSERT INTO students (id, first_name, last_name, birthdate) VALUES (26, 'Иван', 'Иванов', '1994-03-12 00:00:00');
INSERT INTO students (id, first_name, last_name, birthdate) VALUES (2, 'Иван', 'Кузнецов', NULL);
INSERT INTO students (id, first_name, last_name, birthdate) VALUES (27, 'Игнат', 'ГУмнатовичк', '1995-03-12 00:00:00');
INSERT INTO students (id, first_name, last_name, birthdate) VALUES (15, 'Геннадий', 'Малахов', '1997-10-17 00:00:00');


--
-- TOC entry 2123 (class 0 OID 0)
-- Dependencies: 181
-- Name: students_id_seq; Type: SEQUENCE SET; Schema: public; Owner: students_client
--

SELECT pg_catalog.setval('students_id_seq', 27, true);


--
-- TOC entry 2112 (class 0 OID 16409)
-- Dependencies: 184
-- Data for Name: students_marks; Type: TABLE DATA; Schema: public; Owner: students_client
--

INSERT INTO students_marks (id, mark, student_id, date) VALUES (1, 3, 3, NULL);
INSERT INTO students_marks (id, mark, student_id, date) VALUES (3, 5, 1, NULL);
INSERT INTO students_marks (id, mark, student_id, date) VALUES (4, 5, 1, NULL);
INSERT INTO students_marks (id, mark, student_id, date) VALUES (7, 3, 2, '1994-03-12 00:00:00');
INSERT INTO students_marks (id, mark, student_id, date) VALUES (2, 1, 5, NULL);
INSERT INTO students_marks (id, mark, student_id, date) VALUES (8, 3, 5, NULL);
INSERT INTO students_marks (id, mark, student_id, date) VALUES (9, 3, 5, NULL);
INSERT INTO students_marks (id, mark, student_id, date) VALUES (10, 5, 2, '2018-09-30 00:00:00');
INSERT INTO students_marks (id, mark, student_id, date) VALUES (11, 4, 5, '2018-10-01 00:00:00');
INSERT INTO students_marks (id, mark, student_id, date) VALUES (12, 1, 5, '2018-10-01 00:00:00');
INSERT INTO students_marks (id, mark, student_id, date) VALUES (13, 2, 5, '2018-10-07 00:00:00');


--
-- TOC entry 2124 (class 0 OID 0)
-- Dependencies: 183
-- Name: students_marks_id_seq; Type: SEQUENCE SET; Schema: public; Owner: students_client
--

SELECT pg_catalog.setval('students_marks_id_seq', 13, true);


--
-- TOC entry 1994 (class 2606 OID 16414)
-- Name: students_marks_pk; Type: CONSTRAINT; Schema: public; Owner: students_client
--

ALTER TABLE ONLY students_marks
    ADD CONSTRAINT students_marks_pk PRIMARY KEY (id);


--
-- TOC entry 1992 (class 2606 OID 16405)
-- Name: students_pk; Type: CONSTRAINT; Schema: public; Owner: students_client
--

ALTER TABLE ONLY students
    ADD CONSTRAINT students_pk PRIMARY KEY (id);


--
-- TOC entry 1990 (class 1259 OID 16406)
-- Name: students_last_name_idx; Type: INDEX; Schema: public; Owner: students_client
--

CREATE INDEX students_last_name_idx ON students USING btree (last_name);


--
-- TOC entry 2119 (class 0 OID 0)
-- Dependencies: 6
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2018-10-07 20:08:29

--
-- PostgreSQL database dump complete
--

