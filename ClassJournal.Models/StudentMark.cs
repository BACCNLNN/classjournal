﻿using System;
using System.Collections.Generic;

namespace ClassJournal.Models
{
	public class StudentMark
	{
		public int Id { get; set; }
		public int Mark { get; set; }
		public int StudentId { get; set; }
		public DateTime? Date { get; set; }

		public override string ToString()
		{
			return $"{Id} {Mark} {StudentId} {Date:dd.MM.yyyy HH:mm}";
		}

		public static List<StudentMark> SelectAll()
		{
			var result = new List<StudentMark>();
			DataBase.WithCommand(
				"select id,mark,student_id,date from students_marks order by id",
				null,
				command =>
				{
					using (var reader = command.ExecuteReader())
						while (reader.Read())
						{
							var studentMark = new StudentMark
							{
								Id = reader.GetInt32(0),
								Mark = reader.GetInt32(1),
								StudentId = reader.GetInt32(2),
								Date = reader.IsDBNull(3) ? (DateTime?) null : reader.GetDateTime(3)
							};
							result.Add(studentMark);
						}
				});
			return result;
		}

		public static List<StudentMark> SelectByStudentId(int studentId)
		{
			var result = new List<StudentMark>();
			DataBase.WithCommand(
				"select * from students_marks where student_id = @studentId order by id",
				new { studentId },
				command =>
				{
					using (var reader = command.ExecuteReader())
						while (reader.Read())
						{
							var studentMark = new StudentMark
							{
								Id = reader.GetInt32(0),
								Mark = reader.GetInt32(1),
								StudentId = reader.GetInt32(2),
								Date = reader.IsDBNull(3) ? (DateTime?) null : reader.GetDateTime(3)
							};
							result.Add(studentMark);
						}
				});
			return result;
		}

		public static StudentMark GetById(int markId)
		{
			StudentMark result = null;

			DataBase.WithCommand(
				"select * from students_marks where id = @markId",
				new	{ markId },
				command =>
				{
					using (var reader = command.ExecuteReader())
						if (reader.Read())
						{
							result = new StudentMark
							{
								Id = reader.GetInt32(0),
								Mark = reader.GetInt32(1),
								StudentId = reader.GetInt32(2),
								Date = reader.IsDBNull(3) ? (DateTime?) null : reader.GetDateTime(3)
							};
						}
				});
			return result;
		}

		public void Update()
			=> DataBase.WithCommand(
				"update students_marks set mark=@Mark,student_id=@StudentId,date=@Date where id=@Id",
				new
				{
					Mark,
					StudentId,
					Date = Date.HasValue ? (object)Date.Value : DBNull.Value,
					Id
				},
				command => command.ExecuteNonQuery());

		public void Delete()
			=> DataBase.WithCommand(
				"delete from students_marks where id=@Id",
				new { Id },
				command => command.ExecuteNonQuery());

		public void Insert()
			=> DataBase.WithCommand(
				"insert into students_marks (mark,student_id,date) values (@Mark,@StudentId,@Date) returning id",
				new
				{
					Mark,
					StudentId,
					Date = Date.HasValue ? (object)Date.Value : DBNull.Value
				},
				command => Id = (int) command.ExecuteScalar());
	}
}
