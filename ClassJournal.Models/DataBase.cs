﻿using System;
using System.Data;
using Npgsql;

namespace ClassJournal.Models
{
	public static class DataBase
	{
		public static string ConnectionString { get; set; }

		public static IDbConnection CreateConnection()
		{
			var result = new NpgsqlConnection(ConnectionString);
			result.Open();
			return result;
		}

		public static void WithCommand(string commandText, object parameters, Action<IDbCommand> action)
		{
			using (var conn = CreateConnection())
			using (var command = conn.CreateCommand())
			{
				command.CommandText = commandText;
				if (parameters != null)
				{
					var type = parameters.GetType();
					foreach (var propertyInfo in type.GetProperties())
					{
						var value = propertyInfo.GetValue(parameters);
						command.Parameters.Add(new NpgsqlParameter(propertyInfo.Name, value));
					}
				}
				action(command);
			}
		}
	}
}
