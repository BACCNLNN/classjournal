﻿using System;
using System.Collections.Generic;

namespace ClassJournal.Models
{
	public class Student
	{
		public int Id { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public DateTime? Birthdate { get; set; }
		public float? AverageMark { get; set; }
		public DateTime? LastMark { get; set; }

		public override string ToString()
		{
			return $"{Id}\t{FirstName}\t{LastName}\t{Birthdate:dd.MM.yyyy}\t{AverageMark}\t{LastMark:dd.MM.yyyy HH:mm}";
		}

		public static List<Student> SelectAll(StudentsSort sort)
		{
			var sortField = sort.ToString();
			var member = typeof(StudentsSort).GetField(sortField);
			var attributes = member.GetCustomAttributes(typeof(SortAttribute), false);
			var attribute = (SortAttribute)attributes[0];

			var result = new List<Student>();
			DataBase.WithCommand(
				$@"
select id,first_name,last_name,birthdate, 
	(select avg(mark) from students_marks where student_id = students.id) as avg_mark, 
	(select max(date) from students_marks where student_id = students.id) as last_mark  
	from students order by {attribute.FieldName}",
				null,
				command =>
				{
					using (var reader = command.ExecuteReader())
						while (reader.Read())
						{
							var student = new Student
							{
								Id = reader.GetInt32(0),
								FirstName = reader.GetString(1),
								LastName = reader.GetString(2),
								Birthdate = reader.IsDBNull(3) ? (DateTime?) null : reader.GetDateTime(3),
								AverageMark = reader.IsDBNull(4) ? (float?) null : reader.GetFloat(4),
								LastMark = reader.IsDBNull(5) ? (DateTime?) null : reader.GetDateTime(5)
							};
							result.Add(student);
						}
				});
			return result;
		}

		public static Student GetById(int studentId)
		{
			Student result = null;
			DataBase.WithCommand(
				@"
	select id,first_name,last_name,birthdate,
	(select avg(mark) from students_marks where student_id = @studentId) as avg_mark, 
	(select max(date) from students_marks where student_id = @studentId) as last_mark
	from students where id = @studentId",
				new { studentId },
				command =>
				{
					using (var reader = command.ExecuteReader())
						if (reader.Read())
							result = new Student
							{
								Id = reader.GetInt32(0),
								FirstName = reader.GetString(1),
								LastName = reader.GetString(2),
								Birthdate = reader.IsDBNull(3) ? (DateTime?) null : reader.GetDateTime(3),
								AverageMark = reader.IsDBNull(4) ? (float?) null : reader.GetFloat(4),
								LastMark = reader.IsDBNull(5) ? (DateTime?) null : reader.GetDateTime(5)
							};
				});
			return result;
		}

		public void Insert()
			=> DataBase.WithCommand(
				"insert into students (first_name,last_name,birthdate) values (@FirstName,@LastName, @Birthdate) returning id",
				new
				{
					FirstName,
					LastName,
					Birthdate = Birthdate.HasValue ? (object)Birthdate.Value : DBNull.Value
				},
				command => Id = (int) command.ExecuteScalar());
		
		public void Update()
			=> DataBase.WithCommand(
				"update students set first_name=@FirstName,last_name=@LastName,birthdate=@Birthdate where id=@Id",
				new
				{
					Id,
					FirstName,
					LastName,
					Birthdate = Birthdate.HasValue ? (object)Birthdate.Value : DBNull.Value
				}, 
				command => command.ExecuteNonQuery());

		public void Delete()
			=> DataBase.WithCommand("delete from students where id=@Id",
				new { Id },
				command => command.ExecuteNonQuery());

		public StudentMark AddMark(int mark)
		{
			var studMark = new StudentMark
			{
				Mark = mark,
				StudentId = Id,
				Date = DateTime.Now
			};

			studMark.Insert();
			return studMark;
		}

		public List<StudentMark> Marks => StudentMark.SelectByStudentId(Id);

	}
}

