﻿using System;

namespace ClassJournal.Models
{
	public class SortAttribute : Attribute
	{
		public string FieldName;
		public string Name;

		public SortAttribute(string fieldName, string name)
		{
			FieldName = fieldName;
			Name = name;
		}
	}
}