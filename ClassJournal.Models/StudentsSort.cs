namespace ClassJournal.Models
{
	public enum StudentsSort
	{
		[Sort("id", "Id")] Id,
		[Sort("id desc", "Id")] IdDesc,
		[Sort("first_name", "FirstName")] FirstName,
		[Sort("first_name desc", "FirstName")] FirstNameDesc,
		[Sort("last_name", "LastName")] LastName,
		[Sort("last_name desc", "LastName")] LastNameDesc,
		[Sort("birthdate", "Birthdate")] Birthdate,
		[Sort("birthdate desc", "Birthdate")] BirthdateDesc,
		[Sort("avg_mark", "AverageMark")] AverageMark,
		[Sort("avg_mark desc", "AverageMark")] AverageMarkDesc,
		[Sort("last_mark", "LastMark")] LastMark,
		[Sort("last_mark desc", "LastMark")] LastMarkDesc,
	}
}