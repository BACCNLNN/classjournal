﻿using System;
using System.Collections.Generic;

namespace ClassJournal.Models
{
	public class StudentWithMarks
	{
		public int Id { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public DateTime? Birthdate { get; set; }
		public int MarkId { get; set; }
		public int Mark { get; set; }
		public int StudentId { get; set; }
		public DateTime? Date { get; set; }

		public override string ToString()
		{
			return $"{Id} {FirstName} {LastName} {Birthdate:dd.MM.yyyy} {MarkId} {Mark} {StudentId} {Date:dd.MM.yyyy}";
		}

		public static List<StudentWithMarks> SelectAll()
		{
			var result = new List<StudentWithMarks>();

			using (var conn = DataBase.CreateConnection())
			using (var command = conn.CreateCommand())
			{
				command.CommandText = "select * from students, students_marks where students.id = students_marks.id order by students.id";
				using (var reader = command.ExecuteReader())
					while (reader.Read())
					{
						var studentWithMarks = new StudentWithMarks
						{
							Id = reader.GetInt32(0),
							FirstName = reader.GetString(1),
							LastName = reader.GetString(2),
							Birthdate = reader.IsDBNull(3) ? (DateTime?)null : reader.GetDateTime(3),
							MarkId = reader.GetInt32(4),
							Mark = reader.GetInt32(5),
							StudentId = reader.GetInt32(6),
							Date = reader.IsDBNull(7) ? (DateTime?)null : reader.GetDateTime(7)

						};
						result.Add(studentWithMarks);
					}
			}
			return result;
		}
	}
}
